<?php

namespace Webberig\BackendBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class WebberigBackendBundle extends Bundle
{

    public function boot()
    {

        $menuService = Service\Menu::singleton();
        $menuService
//            ->add("home", "webberig_backend_dashboard", null, "Home", 0)
            ->add("users", "webberig_backend_users", null, "Gebruikers", 1)
            ->setActive("home");
    }
}
