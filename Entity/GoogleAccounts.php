<?php

namespace Webberig\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GoogleAccounts
 *
 * @ORM\Table(name="google_accounts")
 * @ORM\Entity
 */
class GoogleAccounts
{
    /**
     * @var string
     *
     * @ORM\Column(name="identity", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $identity;

    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer", nullable=true)
     */
    private $userid;



    /**
     * Get identity
     *
     * @return string 
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     * @return GoogleAccounts
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
    
        return $this;
    }

    /**
     * Get userid
     *
     * @return integer 
     */
    public function getUserid()
    {
        return $this->userid;
    }
}