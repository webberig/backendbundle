<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mathieu
 * Date: 14/02/13
 * Time: 20:41
 * To change this template use File | Settings | File Templates.
 */

namespace Webberig\BackendBundle\Service;


class Menu {

    private static  $_obj;

    private function _construct()
    {
        $this->data = array();
    }

    public static function singleton()
    {
        if (self::$_obj == null)
            self::$_obj = new Menu();

        return self::$_obj;
    }
    private $data;
    private $active;

    public function add($id, $route, $routeParams, $label, $sequence = 0)
    {
        $this->data[] = array(
            "id" => $id,
            "route" => $route,
            "routeParams" => $routeParams == null ? array() : $routeParams,
            "label" => $label,
            "sequence" => $sequence
        );
        return $this;
    }

    public function setActive($id)
    {
        $this->active = $id;
        return $this;
    }

    public function getMenu()
    {
        usort($this->data, function($a, $b)
        {
            if ($a["sequence"] == $b["sequence"])
            {
                return 0;
            }
            else if ($a["sequence"] < $b["sequence"])
            {
                return -1;
            }
            else {
                return 1;
            }
        });

        foreach ($this->data as &$item)
        {
            if ($item["id"]==$this->active)
                $item["isActive"] = true;
            else
                $item["isActive"] = false;
        }

        return $this->data;
    }


}