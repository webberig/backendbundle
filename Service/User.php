<?php
namespace Webberig\BackendBundle\Service;

use Webberig\SharedBundle\Utility\ArrayUtil;

class User
{
    protected $em;
    protected $ef; // Used for password generation

    public function __construct(\Doctrine\ORM\EntityManager $entityManager, \Symfony\Component\Security\Core\Encoder\EncoderFactory  $encoderFactory)
    {
        $this->em = $entityManager;
        $this->ef = $encoderFactory;
    }

    public function getById($id)
    {
        return $this->em
            ->getRepository("WebberigBackendBundle:Users")
            ->findOneBy(array("id" => $id));
    }

    public function getList()
    {
        return $this->em
            ->getRepository("WebberigBackendBundle:Users")
            ->findAll();
    }

    public function delete($id)
    {
        $em = $this->em;
        if ($user = $this->getById($id))
        {
            $em->remove($user);
            $em->flush();
        }
    }

    public function save($user)
    {
        $em = $this->em;
        $em->persist($user);
        $em->flush();
    }
    public function populate($hash, \Webberig\BackendBundle\Entity\Users $user = null)
    {
        if (!ArrayUtil::isArrayType($hash)) {
            throw new \InvalidArgumentException('$data is not an array');
        }

        $user = $user ? : new \Webberig\BackendBundle\Entity\Users();

        foreach ($hash as $key => $value) {
            try {
                switch ($key) {
                    case 'username':
                        $user->setUsername($value);
                        break;
                    case 'password':
                        if (strlen($value)>0)
                        {
                            $user->reSalt();
                            $encoder = $this->ef->getEncoder($user);
                            $password = $encoder->encodePassword($value, $user->getSalt());
                            $user->setPassword($password);
                        }
                        break;
                    case 'fullname':
                        $user->setFullname($value);
                        break;
                }
            } catch (\Exception $e /* TODO make it more specific instead of just Exception */) {
            }
        }

        return $user;
    }

}