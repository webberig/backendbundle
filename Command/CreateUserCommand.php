<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mathieu
 * Date: 17/02/13
 * Time: 16:00
 * To change this template use File | Settings | File Templates.
 */

namespace Webberig\BackendBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUserCommand extends ContainerAwareCommand {
    protected function configure()
    {
        $this
            ->setName('backend:createuser')
            ->setDescription('Maak een nieuwe gebruiker aan')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Gebruikersnaam?'
            )
            ->addArgument(
                'password',
                InputArgument::REQUIRED,
                'Paswoord?'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var \Webberig\BackendBundle\Service\User */
        $userService = $this->getContainer()->get('backend.user');

        /* @var \Webberig\BackendBundle\Entity\Users */
        $user = new \Webberig\BackendBundle\Entity\Users();
        $data = array(
            "username" => $input->getArgument('name'),
            "fullname" => $input->getArgument('name'),
            "password" => $input->getArgument('password')
        );
        $userService->populate($data, $user);
        $userService->save($user);

        $output->writeln("User was created");
    }
}