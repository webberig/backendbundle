<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mathieu
 * Date: 14/02/13
 * Time: 21:46
 * To change this template use File | Settings | File Templates.
 */

namespace Webberig\BackendBundle\Twig;


class MenuExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            "getBackendMenu" => new \Twig_Function_Method($this, "getMenu")
        );
    }
    public function getMenu()
    {
        return \Webberig\BackendBundle\Service\Menu::singleton()->getMenu();
    }

    public function getName()
    {
        return 'backend_menu_extension';
    }
}