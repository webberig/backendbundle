<?php

namespace Webberig\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

class AccountController extends Controller
{
    public function loginAction()
    {
        /*
        // Create a user
        $user = new \Webberig\BackendBundle\Entity\Users();

        $encoder = $this->get('security.encoder_factory')->getEncoder($user);
        $password = $encoder->encodePassword("12345678", $user->getSalt());

        $user->setPassword($password)->setUsername("mathieu@webberig.be")->setFullname("Mathieu TEST")->setRole(1);


        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();


        //////
        */
        $request = $this->getRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render(
            'WebberigBackendBundle:Account:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                'error'         => $error,
            )
        );
    }



}
