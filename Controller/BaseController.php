<?php
namespace Webberig\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

class BaseController extends Controller
{
    /**
     * @return Session
     */
    public function getSession()
    {
        return $this->get('session');
    }

    public function addFlashSuccess($message) {
        $this->getSession()->getFlashBag()->add('success', $message);
    }
    public function addFlashDanger($message) {
        $this->getSession()->getFlashBag()->add('danger', $message);
    }
    public function addFlashInfo($message) {
        $this->getSession()->getFlashBag()->add('info', $message);
    }
}

