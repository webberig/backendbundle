<?php
namespace Webberig\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UsersController extends BaseController
{
    public function indexAction()
    {
        \Webberig\BackendBundle\Service\Menu::singleton()->setActive("users");

        $userService = $this->get('backend.user');
        $users = $userService->getList();

        return $this->render('WebberigBackendBundle:Users:index.html.twig', array('users' => $users));
    }

    public function editAction($id)
    {
        \Webberig\BackendBundle\Service\Menu::singleton()->setActive("users");

        $userService = $this->get('backend.user');
        $user = $userService->getById($id);
        if (!$user)
        {
            return $this->redirect($this->generateUrl("webberig_backend_users"));
        }
        return $this->render('WebberigBackendBundle:Users:edit.html.twig', array('user' => $user));
    }

    public function saveAction($id)
    {
        $userService = $this->get('backend.user');
        $user = $userService->getById($id);

        $request = $this->getRequest()->request;
        $user = $userService->populate($request->all(), $user);

        $validator = $this->get('validator');
        $errors = $validator->validate($user);

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $this->addFlashDanger($error->getMessage());
            }
            return $this->render('WebberigBackendBundle:Users:edit.html.twig', array('user' => $user, 'errors' => $errors));
        } else {
            $userService->save($user);
            $this->addFlashSuccess("Nieuwe gebruiker is aangemaakt");
            return $this->redirect($this->generateUrl("webberig_backend_users"));
        }

    }

    public function newAction()
    {
        \Webberig\BackendBundle\Service\Menu::singleton()->setActive("users");
        $user = new \Webberig\BackendBundle\Entity\Users();
        return $this->render('WebberigBackendBundle:Users:edit.html.twig', array('user' => $user));
    }

    public function deleteAction($id)
    {
        $userService = $this->get('backend.user');
        $userService->delete($id);
        $this->addFlashSuccess("Gebruiker is verwijderd");
        return $this->redirect($this->generateUrl("webberig_backend_users"));
    }
}